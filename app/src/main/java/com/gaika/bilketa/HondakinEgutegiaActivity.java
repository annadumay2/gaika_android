package com.gaika.bilketa;

import java.util.ArrayList;

import com.gaika.bilketa.adapters.EgutegiAdapter;
import com.gaika.bilketa.model.HondakinMota;
import com.gaika.bilketa.utils.DataBaseController;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

import android.app.ListActivity;
import android.os.Bundle;

public class HondakinEgutegiaActivity extends ListActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.egutegi_layout);
        Settings.setToSelectedLocale(getBaseContext());

		DataBaseController dbc = new DataBaseController(this);
		dbc.open();
		ArrayList<HondakinMota> motak = dbc.getHondakinMotak(Settings.AUKERATUTAKO_HERRIA_ID);
		String hasieraOrdua = dbc.getHasieraOrduaByHerriID(Settings.AUKERATUTAKO_HERRIA_ID);
		String bukaeraOrdua = dbc.getBukaeraOrduaByHerriID(Settings.AUKERATUTAKO_HERRIA_ID);
		
		dbc.close();
		
		for(int i = 0; i < motak.size(); i++)
			motak.get(i).setOrdutegia(hasieraOrdua + " - " + bukaeraOrdua);
		
		setListAdapter(new EgutegiAdapter(this, motak));
	}
}