package com.gaika.bilketa;

import com.gaika.bilketa.utils.DataBaseController;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

public class HerriInformazioActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.herri_informazio_layout);
		TextView txtView = (TextView)findViewById(R.id.txt_informazioa);
		txtView.setTypeface(Typeface.createFromAsset(getAssets(), Settings.FONT_URL));
		String herriIzena = "besteak";
        String testua = "This information is not available in English, for more information go to gaikabilketa.blogspot.com";
		
		if(Settings.AUKERATUTAKO_HIZKUNTZA_ID != 5) // Ez bada English
		{
			if(Settings.AUKERATUTAKO_HERRIA_ID != Settings.EDOZEIN_HERRIA_ID)
			{
				DataBaseController dbc = new DataBaseController(getBaseContext());
				dbc.open();
				herriIzena = dbc.getHerriIzenaByID(Settings.AUKERATUTAKO_HERRIA_ID);
		        dbc.close();
		        
		        if (herriIzena.equals("Arbizu") || herriIzena.equals("Bakaiku") || herriIzena.equals("Etxarri_Aranatz") || herriIzena.equals("Iturmendi") || 
		        	herriIzena.equals("Lakuntza") || herriIzena.equals("Olazti") || herriIzena.equals("Uharte_Arakil") || herriIzena.equals("Urdiain"))
		        	herriIzena = "Sakana";
		        else if (herriIzena.equals("Amasa-Villabona"))
		        	herriIzena = "Amasa_Villabona";
			}
			Log.i("GAIKA", "[" + herriIzena + "] herriaren informazioa.");
			testua = getString(getResources().getIdentifier(herriIzena.toLowerCase() + "_info", "string", getPackageName()));
			//txtView.setText(testua);
		}
			

		txtView.setText(Html.fromHtml(testua));
	}
}
