package com.gaika.bilketa;

import java.io.IOException;
import java.util.ArrayList;

import com.gaika.bilketa.model.HondakinMota;
import com.gaika.bilketa.model.Hondakina;
import com.gaika.bilketa.utils.DataBaseController;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HondakinDetailActivity extends Activity
{
	static private RelativeLayout _hondakinLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		Bundle bundle = this.getIntent().getExtras();
		
		if(bundle != null)
		{
			int zaborID = bundle.getInt("zaborID");
			
			setCurrentHondakina(HondakinDetailActivity.this, zaborID, this);
			if(_hondakinLayout != null)
				setContentView(_hondakinLayout);
		}
	}
	
	static public RelativeLayout setCurrentHondakina(Context ctx, int hondakinID, Activity parentActivity)
	{
		DataBaseController dbc = new DataBaseController(ctx);
		dbc.open();
		Hondakina currentHondakina = dbc.getZaborInfo(hondakinID, Settings.AUKERATUTAKO_HERRIA_ID);
		String hasieraOrdua = dbc.getHasieraOrduaByHerriID(Settings.AUKERATUTAKO_HERRIA_ID);
		String bukaeraOrdua = dbc.getBukaeraOrduaByHerriID(Settings.AUKERATUTAKO_HERRIA_ID);
		dbc.close();
		
		if (currentHondakina == null)
			return null;

		ArrayList<HondakinMota> motak = currentHondakina.getMotak();
		HondakinMota mota;
		
		if(_hondakinLayout == null)
			_hondakinLayout = new RelativeLayout(ctx);
		else
			_hondakinLayout.removeAllViews();
		
		ViewGroup vg = (ViewGroup)_hondakinLayout.getParent();
		if(vg != null)
			vg.removeView(_hondakinLayout);
		
		_hondakinLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		//_hondakinLayout.setOrientation(LinearLayout.VERTICAL);
		
		View lineView = null;
		LinearLayout detailLinearLayout = new LinearLayout(ctx);
		detailLinearLayout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		detailLinearLayout.setOrientation(LinearLayout.VERTICAL);
		View detailView = null;
		RelativeLayout.LayoutParams rlp;
		int motakCount = motak.size();
		int lineAltu = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 5, ctx.getResources().getDisplayMetrics());
		
		for(int i = 0; i < motakCount; i++)
		{
			mota = motak.get(i);
			
			detailView = createView(ctx, currentHondakina.getBistaratzekoIzena(), mota.getIzena(), mota.getBistaratzekoIzena(), mota.getBiltzekoEgunakBistaratzeko(ctx), hasieraOrdua + " - " + bukaeraOrdua, mota.getHasOrdutegia());
			detailLinearLayout.addView(detailView);
			if(motakCount > 1 && i < motakCount - 1)
			{
				lineView = new View(ctx);
				rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, lineAltu);
				lineView.setLayoutParams(rlp);
				lineView.setBackgroundColor(color.darker_gray);
				detailLinearLayout.addView(lineView);
			}
		}
		
		RelativeLayout.LayoutParams parentTopRule = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		parentTopRule.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		_hondakinLayout.addView(detailLinearLayout, parentTopRule);
		
		RelativeLayout.LayoutParams parentBottomRule = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		parentBottomRule.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

		return _hondakinLayout;
	}

	static private View createView(Context ctx, String izena, String motaIkono, String mota, String egunak, String ordutegia, boolean orduakErakutsi)
    {
		LayoutInflater vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView;
        ImageView iconImg;
		TextView izenaTxt;
		TextView motaTxt;
		TextView biltzekoEgunaTxt;
		TextView biltzekoOrdutegiaTxt;
        
		switch (Settings.AUKERATUTAKO_HERRIA_ID)
		{
			case 27: // Pasai Donibane
			case 29: // Bergara
			case 31: // Pasai Antxo (zintz)
			case 32: // Eskoriatza
			case 33: // Pasai San Pedro
			case 34: // Pasai Antxo (lok)
			case 35: // Trintxerpe (zintz)
			case 36: // Trintxerpe (lok)
				convertView = vi.inflate(R.layout.errausketa_detail, null);
				izenaTxt = (TextView)convertView.findViewById(R.id.txt_err_zaborIzena);
				motaTxt = (TextView)convertView.findViewById(R.id.txt_err_zaborMota);
				biltzekoEgunaTxt = (TextView)convertView.findViewById(R.id.txt_err_biltzekoEguna);
				biltzekoOrdutegiaTxt = (TextView)convertView.findViewById(R.id.txt_err_biltzekoOrdutegia);
				
				if(izenaTxt != null)
				{
					izenaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					izenaTxt.setText(izena);
				}

				if(motaTxt != null)
				{
					int colorID	= ctx.getResources().getIdentifier(motaIkono, "color", ctx.getPackageName());
					if(colorID == 0)
			    		colorID = R.color.neutroa;

					motaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					motaTxt.setTextColor(ctx.getResources().getColor(colorID));
					motaTxt.setText(mota);
				}

				if(biltzekoEgunaTxt != null)
		        {
					biltzekoEgunaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					biltzekoEgunaTxt.setText(ctx.getString(ctx.getResources().getIdentifier("err_egunak", "string", ctx.getPackageName())));
		        }

				if(biltzekoOrdutegiaTxt != null)
		        {
					biltzekoOrdutegiaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					biltzekoOrdutegiaTxt.setText(ctx.getString(ctx.getResources().getIdentifier("err_ordutegiak", "string", ctx.getPackageName())));
		        }
				break;
			default: 
				convertView = vi.inflate(R.layout.hondakin_detail_item, null);
		    	iconImg = (ImageView)convertView.findViewById(R.id.img_hondakinIcon);
				izenaTxt = (TextView)convertView.findViewById(R.id.txt_zaborIzena);
				motaTxt = (TextView)convertView.findViewById(R.id.txt_zaborMota);
				biltzekoEgunaTxt = (TextView)convertView.findViewById(R.id.txt_biltzekoEguna);
				biltzekoOrdutegiaTxt = (TextView)convertView.findViewById(R.id.txt_biltzekoOrdutegia);
				
				//HondakinDetailDatuak currentHondakina = getItem(position);

				if(iconImg != null)
				{
					int iconID = ctx.getResources().getIdentifier(motaIkono + "_ikono", "drawable", ctx.getPackageName());
					if(iconID != 0)
						iconImg.setImageDrawable(ctx.getResources().getDrawable(iconID));
				}
				
				if(izenaTxt != null)
		        {
					izenaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					izenaTxt.setText(izena);
		        }

				if(motaTxt != null)
				{
					int colorID	= ctx.getResources().getIdentifier(motaIkono, "color", ctx.getPackageName());
					if(colorID == 0)
			    		colorID = R.color.neutroa;

					motaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					motaTxt.setTextColor(ctx.getResources().getColor(colorID));
					motaTxt.setText(mota);
				}

				if(biltzekoEgunaTxt != null && orduakErakutsi && Settings.AUKERATUTAKO_HERRIA_ID > Settings.EDOZEIN_HERRIA_ID)
		        {
					biltzekoEgunaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					biltzekoEgunaTxt.setText(egunak);
		        }

				if(biltzekoOrdutegiaTxt != null && orduakErakutsi && Settings.AUKERATUTAKO_HERRIA_ID > Settings.EDOZEIN_HERRIA_ID)
		        {
					biltzekoOrdutegiaTxt.setTypeface(Typeface.createFromAsset(ctx.getAssets(), Settings.FONT_URL));
					biltzekoOrdutegiaTxt.setText(ordutegia);
		        }
				break;
		}
        
        return convertView;
    }
}
