package com.gaika.bilketa;

import java.util.ArrayList;

import com.gaika.bilketa.model.DataBaseObject;
import com.gaika.bilketa.utils.DataBaseController;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ZeroZaborActivity extends Activity implements OnClickListener
{
	private AlphaAnimation alphaDown;
    private AlphaAnimation alphaUp;
    static private int AURREKO_HERRI_ID;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        DataBaseController dbc = new DataBaseController(ZeroZaborActivity.this);
        dbc.open();
        Settings.AUKERATUTAKO_HERRIA_ID = dbc.getAukeratutakoHerria();
        Settings.AUKERATUTAKO_HIZKUNTZA_ID = dbc.getAukeratutakoHizkuntza();
        dbc.close();
        
        if(Settings.AUKERATUTAKO_HIZKUNTZA_ID == -1)
        	hizkuntzaGaldetu();
        
        Settings.IS_AUKERAKETA_ALDATUA = false;
        AURREKO_HERRI_ID = -1;
        
        Settings.setToSelectedLocale(getBaseContext());
        setContentView(R.layout.main);

    	findViewById(R.id.btn_zerrenda).setOnClickListener(this);
    	//findViewById(R.id.btn_foroa).setOnClickListener(this);
    	findViewById(R.id.btn_egutegia).setOnClickListener(this);
    	findViewById(R.id.btn_info).setOnClickListener(this);
    	findViewById(R.id.btn_settings).setOnClickListener(this);
    	
    	alphaDown = new AlphaAnimation(1.0f, 0.2f);
        alphaUp = new AlphaAnimation(0.2f, 1.0f);
        alphaDown.setDuration(500);
        alphaUp.setDuration(500);
        alphaDown.setFillAfter(true);
        alphaUp.setFillAfter(true);
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    	AURREKO_HERRI_ID = Settings.AUKERATUTAKO_HERRIA_ID;
    }

	public void onClick(View v)
	{
		switch (v.getId()) {
		case R.id.btn_zerrenda:
			startActivity(new Intent(ZeroZaborActivity.this, HondakinZerrendaActivity.class));
			break;
		case R.id.btn_egutegia:
			startActivity(new Intent(ZeroZaborActivity.this, HondakinEgutegiaActivity.class));
			break;
		case R.id.btn_info:
			startActivity(new Intent(ZeroZaborActivity.this, HerriInformazioActivity.class));
			break;
		case R.id.btn_settings:
			startActivity(new Intent(this, Settings.class));
			break;

		default:
			break;
		}
	}
	
	private void hizkuntzaGaldetu()
	{
		DataBaseController dbc = new DataBaseController(ZeroZaborActivity.this);
		dbc.open();
		ArrayList<DataBaseObject> hizkuntzak = dbc.getObjektuak("hizkuntzak", "ORDER BY id ASC");
		dbc.close();
		
		int count = hizkuntzak.size();
		final CharSequence[] entries = new CharSequence[count];
		
		for(int i = 0; i < count; i++)
		{
			entries[i] = hizkuntzak.get(i).getIzena();
		}
		
	    AlertDialog.Builder listBuilder = new AlertDialog.Builder(ZeroZaborActivity.this);
	    listBuilder.setTitle("Hizkuntza aukeratu:");
	    listBuilder.setItems(entries, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int item) {
	        	DataBaseController dbc = new DataBaseController(ZeroZaborActivity.this);
		        dbc.open();
		        Settings.AUKERATUTAKO_HIZKUNTZA_ID = item + 1;
				dbc.setAukeratutakoHizkuntza(Settings.AUKERATUTAKO_HIZKUNTZA_ID);
		        dbc.close();
	        	Settings.setToSelectedLocale(ZeroZaborActivity.this);
	        }
	    });
	    
	    AlertDialog alertList = listBuilder.create();
	    alertList.show();    
	}
}