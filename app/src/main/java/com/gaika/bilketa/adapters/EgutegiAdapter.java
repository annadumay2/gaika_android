package com.gaika.bilketa.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaika.bilketa.model.HondakinMota;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

public class EgutegiAdapter extends ArrayAdapter<HondakinMota> implements Filterable
{
	protected Context _context;
	protected ArrayList<HondakinMota> _items;
	protected int _layout;
	
	public EgutegiAdapter(Context context, ArrayList<HondakinMota> items)
    {
		super(context, R.layout.egutegia_item, items);
        
        _context = context;
        _items = items;
        _layout = R.layout.egutegia_item;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
    	if (convertView == null)
        {
            LayoutInflater vi = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(_layout, null);
        }

    	ImageView iconImg = (ImageView)convertView.findViewById(R.id.img_hondakinIcon);
		TextView motaTxt = (TextView)convertView.findViewById(R.id.txt_zaborMota);
		TextView biltzekoEgunaTxt = (TextView)convertView.findViewById(R.id.txt_biltzekoEguna);
		TextView biltzekoOrdutegiaTxt = (TextView)convertView.findViewById(R.id.txt_biltzekoOrdutegia);
		
		HondakinMota mota = _items.get(position);
		
		if(iconImg != null)
		{
			int iconID = _context.getResources().getIdentifier(mota.getIzena() + "_ikono", "drawable", _context.getPackageName());
			if(iconID != 0)
			{
				iconImg.setVisibility(View.VISIBLE);
				iconImg.setImageDrawable(_context.getResources().getDrawable(iconID));
			}
			else
				iconImg.setVisibility(View.INVISIBLE);
		}

		if(motaTxt != null)
		{
			int colorID	= _context.getResources().getIdentifier(mota.getIzena(), "color", _context.getPackageName());
			if(colorID == 0)
	    		colorID = R.color.neutroa;

			motaTxt.setTypeface(Typeface.createFromAsset(_context.getAssets(), Settings.FONT_URL));
			motaTxt.setTextColor(_context.getResources().getColor(colorID));
			motaTxt.setText(mota.getBistaratzekoIzena());
		}

		if(biltzekoEgunaTxt != null)
        {
			if(mota.getHasOrdutegia() && Settings.AUKERATUTAKO_HERRIA_ID > Settings.EDOZEIN_HERRIA_ID)
				biltzekoEgunaTxt.setVisibility(View.VISIBLE);
			else
				biltzekoEgunaTxt.setVisibility(View.INVISIBLE);
			
			biltzekoEgunaTxt.setTypeface(Typeface.createFromAsset(_context.getAssets(), Settings.FONT_URL));
			biltzekoEgunaTxt.setText(mota.getBiltzekoEgunakBistaratzeko(_context));
        }

		if(biltzekoOrdutegiaTxt != null)
        {
			if(mota.getHasOrdutegia() && Settings.AUKERATUTAKO_HERRIA_ID > Settings.EDOZEIN_HERRIA_ID)
				biltzekoOrdutegiaTxt.setVisibility(View.VISIBLE);
			else
				biltzekoOrdutegiaTxt.setVisibility(View.INVISIBLE);
			
			biltzekoOrdutegiaTxt.setTypeface(Typeface.createFromAsset(_context.getAssets(), Settings.FONT_URL));
			biltzekoOrdutegiaTxt.setText(mota.getOrdutegia());
        }
        
        return convertView;
    }
}
