package com.gaika.bilketa.adapters;

import java.util.ArrayList;

import com.gaika.bilketa.model.DataBaseObject;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DataBaseObjectAdapter extends ArrayAdapter<DataBaseObject>
{
	protected Context _context;
	protected ArrayList<DataBaseObject> _items;
	protected ArrayList<DataBaseObject> _filteredItems;
	protected int _layout;

    public DataBaseObjectAdapter(Context context, ArrayList<DataBaseObject> items)
    {
        this(context, R.layout.herri_spinner_row, items);
    }
    
    public DataBaseObjectAdapter(Context context, int layout, ArrayList<DataBaseObject> items)
    {
        super(context, layout, items);
        
        _context = context;
        _items = items;
        _filteredItems = new ArrayList<DataBaseObject>(items);
        _layout = layout;
    }

    @Override
    public DataBaseObject getItem(int position)
    {
    	return _filteredItems.get(position);
    }
    
    @Override
    public int getCount()
    {
    	return _filteredItems.size();
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
    	if (convertView == null)
        {
            LayoutInflater vi = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(_layout, null);
        }
    	
        TextView txt = (TextView)convertView.findViewById(R.id.spinner_item_id);
        if(txt != null)
        {
        	txt.setTypeface(Typeface.createFromAsset(_context.getAssets(), Settings.FONT_URL));
        	txt.setText(_filteredItems.get(position).getBistaratzekoIzena());
        }
        
        return convertView;
    }
    
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
    	if (convertView == null)
        {
            LayoutInflater vi = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(_layout, null);
        }

        TextView txt = (TextView)convertView.findViewById(R.id.spinner_item_id);
        if(txt != null)
        {
        	txt.setTypeface(Typeface.createFromAsset(_context.getAssets(), Settings.FONT_URL));
        	txt.setText(_filteredItems.get(position).getBistaratzekoIzena());
        }
        
        return convertView;
    }

	public int getPositionByID(int aukeraID)
	{
		for (int i = 0; i < _items.size(); i++)
		{
			if(_items.get(i).getID() == aukeraID)
				return i;
		}
		
		return -1;
	}
    
    @SuppressWarnings("unchecked")
    public void setFilter(String filter)
    {
    	_filteredItems.clear();
    	
    	if(filter == "")
    	{
    		_filteredItems = (ArrayList<DataBaseObject>)_items.clone();
    		this.notifyDataSetChanged();
    		return;
    	}
    	
    	DataBaseObject zaborra;
    	
    	for (int i = 0; i < _items.size(); i++)
    	{
    		zaborra = _items.get(i);
    		
    		if(zaborra.getBistaratzekoIzena().indexOf(filter) != -1)
    			_filteredItems.add(zaborra);
		}
    	
    	this.notifyDataSetChanged();
    }
}