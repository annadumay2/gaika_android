package com.gaika.bilketa;

import com.gaika.bilketa.adapters.HondakinZerrendaAdapter;
import com.gaika.bilketa.model.DataBaseObject;
import com.gaika.bilketa.utils.DataBaseController;
import com.gaika.bilketa.utils.Settings;
import com.gaika.bilketa.R;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HondakinZerrendaActivity extends ListActivity implements OnItemClickListener, TextWatcher, OnClickListener
{
	static private HondakinZerrendaAdapter ZABOR_ADAPTER;
	private EditText _bilaketaTxt;
	private ProgressDialog _progressDialog;
	private LinearLayout _detailLayout;
	private Button _laguneiGaldetu;
	static final int HONDAKINAK_LORTUAK = 0;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zerrenda);
        
        Settings.setToSelectedLocale(getBaseContext());
        if(ZABOR_ADAPTER == null || Settings.IS_AUKERAKETA_ALDATUA)
        {
        	Runnable lortuHondakinak = new Runnable()
    	    {
    	    	public void run()
    	    	{
    	    		DataBaseController dbc = new DataBaseController(HondakinZerrendaActivity.this);
    	            dbc.open();
    	            ZABOR_ADAPTER = new HondakinZerrendaAdapter(HondakinZerrendaActivity.this, dbc.getHondakinak());
    	            dbc.close();

    	    	    Message msg = new Message();
    	    	    msg.what = HONDAKINAK_LORTUAK;
    	    	    myHandler.sendMessageDelayed(msg, HONDAKINAK_LORTUAK);
    	    	}
    	    };
            
            Thread thread =  new Thread(null, lortuHondakinak, "MagentoBackground");
            thread.start();

            _progressDialog = ProgressDialog.show(HondakinZerrendaActivity.this, getString(R.string.espetu), getString(R.string.hondakinak_kargatzen), true, false);
        }
        else
        {
    		ZABOR_ADAPTER.getFilter().filter("");
			setListAdapter(ZABOR_ADAPTER);
	        getListView().setOnItemClickListener(HondakinZerrendaActivity.this);
        }

		_bilaketaTxt = (EditText)findViewById(R.id.txt_bilaketa);
		_bilaketaTxt.setHint(R.string.hodakina_bilatu_hint);
	    _bilaketaTxt.setTypeface(Typeface.createFromAsset(getAssets(), Settings.FONT_URL));
        _bilaketaTxt.addTextChangedListener(HondakinZerrendaActivity.this);

        _laguneiGaldetu = (Button)findViewById(R.id.btn_laguneiGaldetu);
        _laguneiGaldetu.setOnClickListener(this);
        _laguneiGaldetu.setVisibility(View.INVISIBLE);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }
	
	private Handler myHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case HONDAKINAK_LORTUAK:
					setListAdapter(ZABOR_ADAPTER);
			        getListView().setOnItemClickListener(HondakinZerrendaActivity.this);

		            if(_progressDialog != null)
		            	_progressDialog.dismiss();
					break;
			}
		}
	};

	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		DataBaseObject zaborra = ZABOR_ADAPTER.getItem(position);
		
		if (getResources().getBoolean(R.bool.has_two_panes))
		{
			_detailLayout = (LinearLayout)findViewById(R.id.layout_hondakinDetail);
			RelativeLayout hondakinLayout = HondakinDetailActivity.setCurrentHondakina(HondakinZerrendaActivity.this, zaborra.getID(), this);
			if(hondakinLayout != null && _detailLayout != null)
				_detailLayout.addView(hondakinLayout);
		}
		else
		{
			Intent intent = new Intent(HondakinZerrendaActivity.this, HondakinDetailActivity.class);
			intent.putExtra("zaborID", zaborra.getID());
			
			startActivity(intent);
		}
	}

	public void afterTextChanged(Editable s)
	{
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
	}

	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		if(ZABOR_ADAPTER != null)
			ZABOR_ADAPTER.getFilter().filter(s, new Filter.FilterListener() {
												    public void onFilterComplete(int count) {
												         if(count < 5)
															_laguneiGaldetu.setVisibility(View.VISIBLE);
														else
															_laguneiGaldetu.setVisibility(View.INVISIBLE);
												    }
												});
	}
	
	@Override
	protected void onDestroy()
	{
		if(_bilaketaTxt != null)
			_bilaketaTxt.removeTextChangedListener(this);
	    super.onDestroy();
	}
	@Override
	public void onPause()
	{
	    super.onPause();

	    if(_progressDialog != null)
	    	_progressDialog.dismiss();
	    _progressDialog = null;
	}

	public void onClick(View v)
	{

		String shareBody = "#gaikazalantza-> " + _bilaketaTxt.getText();

		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"gaikabilketa@gmail.com"});
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Gaika zalantza");
		sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.lagunei_galdetu)));
	}
}