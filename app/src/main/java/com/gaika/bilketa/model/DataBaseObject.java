package com.gaika.bilketa.model;

import android.content.Context;

public class DataBaseObject
{
	private int _id;
	private String _izena;
	private String _bistaratzekoIzena;

	public DataBaseObject(Context ctx, int zID, String izena)
	{
		this(ctx, zID, izena, "");
	}

	public DataBaseObject(Context ctx, int zID, String izena, String bistaratzekoIzena)
	{
		_id = zID;
		_izena = izena;

		if (bistaratzekoIzena == "")
		{
			int hondakinStringKey = ctx.getResources().getIdentifier(izena, "string", ctx.getPackageName());
			
			if(hondakinStringKey != 0)
				_bistaratzekoIzena = ctx.getString(hondakinStringKey);
	    	else
	    		_bistaratzekoIzena = izena.replace("_", " ");
		}
		else
			_bistaratzekoIzena = bistaratzekoIzena;
	}
	
	public int getID()
	{
		return _id;
	}
	
	public String getIzena()
	{
		return _izena;
	}
	
	public String getBistaratzekoIzena()
	{
		return _bistaratzekoIzena;
	}
}
