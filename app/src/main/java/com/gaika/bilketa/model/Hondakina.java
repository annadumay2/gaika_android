package com.gaika.bilketa.model;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;

public class Hondakina extends DataBaseObject
{
	private ArrayList<HondakinMota> _motak;

	public Hondakina(Context ctx, int zID, String izena)
	{
		this(ctx, zID, izena, new ArrayList<HondakinMota>());
	}
	
	public Hondakina(Context ctx, int zID, String izena, HondakinMota mota)
	{
		this(ctx, zID, izena, new ArrayList<HondakinMota>(Arrays.asList(mota)));
	}
	
	public Hondakina(Context ctx, int zID, String izena, ArrayList<HondakinMota> motak)
	{
		super(ctx, zID, izena);
		
		_motak = motak;
	}
	
	public ArrayList<HondakinMota> getMotak()
	{
		return _motak;
	}
	
	public void addMota(HondakinMota newMota)
	{
		_motak.add(newMota);
	}
	
	public void setMotak(ArrayList<HondakinMota> newMota)
	{
		_motak = newMota;
	}
}
