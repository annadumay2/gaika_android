package com.gaika.bilketa.model;

import android.content.Context;

public class Hizkuntza extends DataBaseObject
{
	private String _laburdura;
	
	public Hizkuntza(Context ctx, int id, String laburdura, String izena)
	{
		super(ctx, id, izena);
		
		_laburdura = laburdura;
	}
	
	public String getLaburdura()
	{
		return _laburdura;
	}
}
