package com.gaika.bilketa.model;

import java.util.ArrayList;

import android.content.Context;

import com.gaika.bilketa.R;

public class HondakinMota extends DataBaseObject
{
	private  ArrayList<String> _biltzekoEgunak;
	private String _ordutegia;
	private boolean _hasOrdutegia;
	
	public HondakinMota(Context ctx, int zmID, String izena)
	{
		this(ctx, zmID, izena, null, false);
	}
	
	public HondakinMota(Context ctx, int zmID, String izena, ArrayList<String> biltzekoEgunak, boolean hasOrdutegia)
	{
		super(ctx, zmID, izena);
		_biltzekoEgunak = biltzekoEgunak;
		_ordutegia = "";
		_hasOrdutegia = hasOrdutegia;
	}
	
	public  ArrayList<String> getBiltzekoEgunak()
	{
		return _biltzekoEgunak;
	}

	public String getBiltzekoEgunakBistaratzeko(Context ctx)
	{
		String egunakString = "";
		int size = _biltzekoEgunak.size();
		
		for (int i = 0; i < size; i++)
		{
			egunakString += ctx.getString(ctx.getResources().getIdentifier(_biltzekoEgunak.get(i), "string", ctx.getPackageName()));
			
			if(i < size - 2)
				egunakString += ", ";
			else if (i == size - 2)
				egunakString += " " + ctx.getString(R.string.eta) + " ";
		}
		
		return egunakString;
	}
	
	public String getOrdutegia()
	{
		return _ordutegia;
	}
	
	public boolean getHasOrdutegia()
	{
		return _hasOrdutegia && !_biltzekoEgunak.get(0).equals("egunero");
	}
	
	public void setOrdutegia(String newOrduak)
	{
		_ordutegia = newOrduak;
	}
	
	public void setHasOrdutegia(boolean newHasOrdutegia)
	{
		_hasOrdutegia = newHasOrdutegia;
	}
}
