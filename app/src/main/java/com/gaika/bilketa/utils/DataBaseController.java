package com.gaika.bilketa.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.gaika.bilketa.model.DataBaseObject;
import com.gaika.bilketa.model.Hizkuntza;
import com.gaika.bilketa.model.HondakinMota;
import com.gaika.bilketa.model.Hondakina;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class DataBaseController extends SQLiteOpenHelper
{
	private SQLiteDatabase mDb;
    private Context myContext;
    
    private static String DB_PATH;
    private static final String DATABASE_NAME = "zerozabor.db";
    private static final int DATABASE_VERSION = 16; //Manifest-eko 'Version code'ren berdina izan behar du
    
    private static int AURREKO_HERRI_ID;
    private static int AURREKO_HIZKUNTZA_ID;
    private static boolean IS_DB_UPGRADED = false;
    
    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     * 
     * @param ctx the Context within which to work
     */
    public DataBaseController(Context ctx)
    {
    	super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    	
    	if(IS_DB_UPGRADED)
    	{
    		IS_DB_UPGRADED = false;
    		open();
    		setAukeratutakoHerria(AURREKO_HERRI_ID);
    		setAukeratutakoHizkuntza(AURREKO_HIZKUNTZA_ID);
    		close();
    	}
    	
    	this.myContext = ctx;
    	DB_PATH = "/data/data/" + this.myContext.getPackageName() + "/databases/";
    }
    
    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     * 
     * @return this (self reference, allowing this to be chained in an
     *         initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public DataBaseController open() throws SQLException
    {
		try{
	    	mDb = null;
			
			if(!this.isDataBaseExist())
			{
				//File dbFile = new File(DB_PATH+DATABASE_NAME);
				//dbFile.delete();
				
				//get database, we will override it in next steep
				//but folders containing the database are created
				mDb = this.getReadableDatabase();
				mDb.close();
				//copy database
				this.copyDataBase();                              
			}
			
		mDb = this.getWritableDatabase();
		
		}catch(SQLException eSQL){
			Log.e("log_tag","Can not open database");
		}
		catch (IOException IOe) {
			Log.e("log_tag","Can not copy initial database");
		}
        return this;
    }
    
    public ArrayList<DataBaseObject> getObjektuak(String taula)
    {
    	return getObjektuak(taula, "ORDER BY izena ASC");
    }
    
    public ArrayList<DataBaseObject> getObjektuak(String taula, String orderBy)
    {
    	String sql = "SELECT * FROM " + taula + " " + orderBy;
        Cursor cursor = mDb.rawQuery(sql, null);

		ArrayList<DataBaseObject> dataBaseObjects = new ArrayList<DataBaseObject>();
		if (cursor != null && cursor.moveToFirst())
		{
			int idZutabe = cursor.getColumnIndexOrThrow("ID");
			int izenZutabe = cursor.getColumnIndexOrThrow("izena");
			String izena;
			String bistaIzena;
			String luzapena;
			do
			{
				izena = cursor.getString(izenZutabe);
				bistaIzena = "";
				
				if(izena.indexOf("_lok") != -1 || izena.indexOf("_zintz") != -1)
				{
					if(izena.indexOf("_lok") != -1)
						luzapena = "_lok";
					else
						luzapena = "_zintz";
					
					int lekuStringKey = myContext.getResources().getIdentifier(luzapena, "string", myContext.getPackageName());
					
					if(lekuStringKey != 0)
						bistaIzena = izena.replace(luzapena, " (") + myContext.getString(lekuStringKey) + ")";
					
					bistaIzena = bistaIzena.replace("_", " ");
				}
				
				dataBaseObjects.add(new DataBaseObject(myContext, cursor.getInt(idZutabe), izena, bistaIzena));
			} while(cursor.moveToNext());

		     cursor.close();
		}
    	
    	return dataBaseObjects;
    }
    
    public ArrayList<Hondakina> getHondakinak()
    {
    	ArrayList<Hondakina> dataBaseObjects = new ArrayList<Hondakina>();
		String sql = "SELECT * FROM zaborrak ORDER BY izena ASC";
        Cursor cursor = mDb.rawQuery(sql, null);
		if (cursor != null && cursor.moveToFirst())
		{
			int idZutabe = cursor.getColumnIndexOrThrow("ID");
			int herriID = getAukeratutakoHerria();
			do
			{
				dataBaseObjects.add(getZaborInfo(cursor.getInt(idZutabe), herriID));
			} while(cursor.moveToNext());

		     cursor.close();
		}
    	return dataBaseObjects;
    }

	public ArrayList<HondakinMota> getHondakinMotak(int herriID)
	{
		String sql;
		Cursor cursor;
		
		SQLiteStatement stmt = mDb.compileStatement("SELECT MAX(ID) FROM zabor_motak");
        int motaCount = (int) stmt.simpleQueryForLong();
		
		ArrayList<HondakinMota> motak = new ArrayList<HondakinMota>();
		
		int motaID;
		ArrayList<String> biltzekoEgunak;
		String[] egunIDs;
		String motaIzena;
		boolean hasOrdutegia;
		
		for (int i = 1; i < motaCount; i++)
		{
			motaID = i;

			if(herriID == Settings.EDOZEIN_HERRIA_ID)
				sql = "SELECT zabor_motak.izena AS mota_izena, zabor_motak.ID AS mota_id, zabor_motak.has_ordutegia AS has_ordutegia "
					+ "FROM zabor_motak WHERE mota_id = " + motaID;
			else
				sql = "SELECT zabor_motak.izena AS mota_izena, zabor_motak.ID AS mota_id, zabor_motak.has_ordutegia AS has_ordutegia, egun_antolaketa.egun_id AS egunak "
					+ "FROM zabor_motak INNER JOIN egun_antolaketa ON egun_antolaketa.herri_id = " + herriID + " AND egun_antolaketa.zabor_mota_id = zabor_motak.ID WHERE mota_id = " + motaID;
			
			biltzekoEgunak = new ArrayList<String>();
			cursor = mDb.rawQuery(sql, null);
	    	
	    	if (cursor != null && cursor.moveToFirst())
	    	{
	    		motaIzena = cursor.getString(cursor.getColumnIndexOrThrow("mota_izena"));

	    		if(herriID == Settings.EDOZEIN_HERRIA_ID)
	    		{
    				hasOrdutegia = false;
	    			egunIDs = new String[0];
	    		}
    			else
    			{
    				hasOrdutegia = cursor.getInt(cursor.getColumnIndexOrThrow("has_ordutegia")) == 1;
    				egunIDs = cursor.getString(cursor.getColumnIndexOrThrow("egunak")).split(",");
    			}
	    		
	    		cursor.close();
	    		for (int j = 0; j < egunIDs.length; j++)
	    		{
					sql = "SELECT izena FROM egunak WHERE ID = " + egunIDs[j];
					cursor = mDb.rawQuery(sql, null);
	    	
			    	if (cursor != null && cursor.moveToFirst())
			    	{
						biltzekoEgunak.add(cursor.getString(cursor.getColumnIndexOrThrow("izena")));
			    		
			    		cursor.close();
			    	}
				}
	    		
	    		motak.add(new HondakinMota(myContext, motaID, motaIzena, biltzekoEgunak, hasOrdutegia));
	    	}
		}
		
		return motak;
	}
    
    public int getAukeratutakoHerria()
    {
    	Cursor cursor = mDb.rawQuery("SELECT aukeratutako_herri_id FROM aukerak", null);
    	int itzuli = -1;
    	
    	if (cursor != null && cursor.moveToFirst())
    		itzuli = cursor.getInt(0);
    		
    	cursor.close();
    	
    	return itzuli;
    }
    
    public void setAukeratutakoHerria(int herriID)
    {
		String sql = "UPDATE aukerak SET aukeratutako_herri_id = " + herriID;
		Log.i("DataBaseController", sql);
		mDb.execSQL(sql);
    }
    
    public int getAukeratutakoHizkuntza()
    {
    	Cursor cursor = mDb.rawQuery("SELECT aukeratutako_hizkuntza_id FROM aukerak", null);
    	int itzuli = -1;
    	
    	if (cursor != null && cursor.moveToFirst())
    		itzuli = cursor.getInt(0);
    		
    	cursor.close();
    	
    	return itzuli;
    }
    
    public void setAukeratutakoHizkuntza(int hizkuntzaID)
    {
		String sql = "UPDATE aukerak SET aukeratutako_hizkuntza_id = " + hizkuntzaID;
		Log.i("DataBaseController", sql);
		mDb.execSQL(sql);
    }

	public String getHasieraOrduaByHerriID(int herriID)
	{
		Cursor cursor = mDb.rawQuery("SELECT hasiera_ordua FROM herriak WHERE ID = " + herriID, null);
    	String itzuli = "";
    	
    	if (cursor != null && cursor.moveToFirst())
    		itzuli = cursor.getString(0);
    		
    	cursor.close();
    	
    	return itzuli;
	}

	public String getBukaeraOrduaByHerriID(int herriID)
	{
		Cursor cursor = mDb.rawQuery("SELECT bukaera_ordua FROM herriak WHERE ID = " + herriID, null);
    	String itzuli = "";
    	
    	if (cursor != null && cursor.moveToFirst())
    		itzuli = cursor.getString(0);
    		
    	cursor.close();
    	
    	return itzuli;
	}

	public String getHerriIzenaByID(int herriID)
	{
		Cursor cursor = mDb.rawQuery("SELECT izena FROM herriak WHERE ID = " + herriID, null);
    	String bistaIzena = "";
    	String luzapena;
    	String izena = "";
    	
    	if (cursor != null && cursor.moveToFirst())
    		izena = cursor.getString(0);
    		
    	cursor.close();
    	
		if(izena.indexOf("_lok") != -1 || izena.indexOf("_zintz") != -1)
		{
			if(izena.indexOf("_lok") != -1)
				luzapena = "_lok";
			else
				luzapena = "_zintz";
			
			int lekuStringKey = myContext.getResources().getIdentifier(luzapena, "string", myContext.getPackageName());
			
			if(lekuStringKey != 0)
				bistaIzena = izena.replace(luzapena, " (") + myContext.getString(lekuStringKey) + ")";
		}
		else
			bistaIzena = izena;
		
		bistaIzena = bistaIzena.replace("_", " ");
    	
    	return bistaIzena;
	}

	public Hizkuntza getHizkuntzaByID(int hizkuntzaID)
	{
		Cursor cursor = mDb.rawQuery("SELECT * FROM hizkuntzak WHERE ID = " + hizkuntzaID, null);
    	Hizkuntza itzuli = null;
    	
    	if (cursor != null && cursor.moveToFirst())
    		itzuli = new Hizkuntza(myContext, cursor.getInt(cursor.getColumnIndex("ID")), cursor.getString(cursor.getColumnIndex("laburdura")), cursor.getString(cursor.getColumnIndex("izena")));
    		
    	cursor.close();
    	
    	return itzuli;
	}

	public Hondakina getZaborInfo(int zaborID, int herriID)
	{
		Hondakina zaborra = null;
		
		String sql = "SELECT zaborrak.izena AS zabor_izena, zaborrak.zabor_mota_id AS mota_idak FROM zaborrak WHERE zaborrak.ID = " + zaborID;
		
    	Cursor cursor = mDb.rawQuery(sql, null);

    	if (cursor != null)
    	{
    		String motaIzena = "";
    		//int motaID = 0;
			//int motaZutabe = cursor.getColumnIndexOrThrow("mota_izena");
			//int egunZutabe = cursor.getColumnIndexOrThrow("egunak");
    		String[] motaIDs = {};
			
    		if(cursor.moveToFirst())
    		{
				int motaIdZutabe = cursor.getColumnIndexOrThrow("mota_idak");
				int zaborIzenZutabe = cursor.getColumnIndexOrThrow("zabor_izena");
    			String motakString = cursor.getString(motaIdZutabe);
    			
    			//egunIDs = cursor.getString(egunZutabe).split(",");
    			if(motakString.indexOf(",") != -1)
    				motaIDs = motakString.split(",");
    			else
    				motaIDs = new String[]{motakString};
    			//motaID = cursor.getInt(idZutabe);
    			//motaIzena = cursor.getString(motaZutabe);
    			zaborra = new Hondakina(myContext, zaborID, cursor.getString(zaborIzenZutabe));//, new ZaborMota(cursor.getString(motaZutabe), cursor.getString(herriZutabe)));
    		}
    		
    		cursor.close();
    		
    		int motaID;
    		ArrayList<String> biltzekoEgunak;
    		String[] egunIDs;
    		boolean hasOrdutegia;
    		
    		for (int i = 0; i < motaIDs.length; i++)
    		{
    			motaID = Integer.parseInt(motaIDs[i]);

				//sql = "SELECT izena FROM egunak WHERE ID = " + egunIDs[i];
    			if(herriID == Settings.EDOZEIN_HERRIA_ID)
    				sql = "SELECT zabor_motak.izena AS mota_izena, zabor_motak.ID AS mota_id, zabor_motak.has_ordutegia AS has_ordutegia "
    					+ "FROM zabor_motak WHERE mota_id = " + motaID;
    			else
    				sql = "SELECT zabor_motak.izena AS mota_izena, zabor_motak.ID AS mota_id, zabor_motak.has_ordutegia AS has_ordutegia, egun_antolaketa.egun_id AS egunak "
    					+ "FROM zabor_motak INNER JOIN egun_antolaketa ON egun_antolaketa.herri_id = " + herriID + " AND egun_antolaketa.zabor_mota_id = zabor_motak.ID WHERE mota_id = " + motaID;
    			
    			biltzekoEgunak = new ArrayList<String>();
    			cursor = mDb.rawQuery(sql, null);
    	    	
		    	if (cursor != null && cursor.moveToFirst())
		    	{
		    		motaIzena = cursor.getString(cursor.getColumnIndexOrThrow("mota_izena"));
		    		if(herriID == Settings.EDOZEIN_HERRIA_ID)
		    		{
	    				hasOrdutegia = false;
		    			egunIDs = new String[0];
		    		}
	    			else
	    			{
	    				hasOrdutegia = cursor.getInt(cursor.getColumnIndexOrThrow("has_ordutegia")) == 1;
	    				egunIDs = cursor.getString(cursor.getColumnIndexOrThrow("egunak")).split(",");
	    			}
		    		
		    		cursor.close();
		    		for (int j = 0; j < egunIDs.length; j++)
		    		{
						sql = "SELECT izena FROM egunak WHERE ID = " + egunIDs[j];
						cursor = mDb.rawQuery(sql, null);
		    	
				    	if (cursor != null && cursor.moveToFirst())
				    	{
							biltzekoEgunak.add(cursor.getString(cursor.getColumnIndexOrThrow("izena")));
				    		
				    		cursor.close();
				    	}
					}
		    		
		    		if(zaborra != null)
		    			zaborra.addMota(new HondakinMota(myContext, motaID, motaIzena, biltzekoEgunak, hasOrdutegia));
		    	}
    		}
    	}
		
		return zaborra;
	}
    
    private boolean isDataBaseExist()
    {
        File dbFile = new File(DB_PATH + DATABASE_NAME);
        return dbFile.exists();
    }
    
    private void copyDataBase() throws IOException
    {
        InputStream myInput = myContext.getAssets().open("databases/" + DATABASE_NAME);
        String outFileName = DB_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0)
        {
        	myOutput.write(buffer, 0, length);
        }
        
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    @Override
	public synchronized void close()
    {
    	if(mDb != null)
    		mDb.close();

    	super.close();
	}
    
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        //db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
    	Log.v("josu", "oldVersion: " + oldVersion + " newVersion: " + newVersion);  
		if (oldVersion < newVersion)
		{ 
		    try
		    {
		    	Cursor cursor = db.rawQuery("SELECT aukeratutako_hizkuntza_id FROM aukerak", null);
		    	AURREKO_HIZKUNTZA_ID = -1; 
		    	if (cursor != null && cursor.moveToFirst())
		    		AURREKO_HIZKUNTZA_ID = cursor.getInt(0);
		    	cursor.close();

		    	cursor = db.rawQuery("SELECT aukeratutako_herri_id FROM aukerak", null);
		    	AURREKO_HERRI_ID = -1; 
		    	if (cursor != null && cursor.moveToFirst())
		    		AURREKO_HERRI_ID = cursor.getInt(0);
		    	cursor.close();
		    	
				File dbFile = new File(DB_PATH+DATABASE_NAME);
				dbFile.delete();
				copyDataBase();
				IS_DB_UPGRADED = true;
		    }
		    catch (IOException e)
		    { 
		        throw new Error("Error upgrading database: " + e.getMessage()); 
		    } 
		}
    }
}
