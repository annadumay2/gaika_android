package com.gaika.bilketa.utils;

import java.util.ArrayList;
import java.util.Locale;

import com.gaika.bilketa.model.DataBaseObject;
import com.gaika.bilketa.model.Hizkuntza;
import com.gaika.bilketa.R;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class Settings extends PreferenceActivity implements OnPreferenceChangeListener
{
	static public int EDOZEIN_HERRIA_ID;
	static public int AUKERATUTAKO_HERRIA_ID;
	static public int AUKERATUTAKO_HIZKUNTZA_ID;
	static public boolean IS_AUKERAKETA_ALDATUA;
	static public final String FONT_URL = "fonts/PTSans.ttf";
	static public final String BOLD_FONT_URL = "fonts/PTSansBold.ttf";

	private ListPreference _herriakList;
	private ListPreference _hizkuntzaList;
	private DataBaseController dbc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);

		IS_AUKERAKETA_ALDATUA = false;
		dbc = new DataBaseController(Settings.this);
		dbc.open();
		ArrayList<DataBaseObject> herriak = dbc.getObjektuak("herriak");
		AUKERATUTAKO_HERRIA_ID = dbc.getAukeratutakoHerria();
		AUKERATUTAKO_HIZKUNTZA_ID = dbc.getAukeratutakoHizkuntza();
		dbc.close();
		//_herriSpinner.setAdapter(herriAdapter);
		int count = herriak.size();
		CharSequence[] entries = new CharSequence[count + 1];
		CharSequence[] entryValues = new CharSequence[count + 1];
		DataBaseObject herria;
		String aukeratutakoIzena = "";
		
		entries[0] = getString(R.string.beste_herriak);
		entryValues[0] = String.valueOf(EDOZEIN_HERRIA_ID);

		for(int i = 0; i < count; i++)
		{
			herria = herriak.get(i);
			
			if(herria.getID() == AUKERATUTAKO_HERRIA_ID)
				aukeratutakoIzena = herria.getBistaratzekoIzena();
			
			entries[i + 1] = herria.getBistaratzekoIzena();
			entryValues[i + 1] = String.valueOf(herria.getID());
		}

		_herriakList = (ListPreference)getPreferenceManager().findPreference("herriaPref");
		//FontHelper.applyFont(getApplicationContext(), findViewById(_herriakList.getLayoutResource()), FONT_URL);
		_herriakList.setOnPreferenceChangeListener(this);
		_herriakList.setEntries(entries);
		_herriakList.setEntryValues(entryValues);

		_herriakList.setDefaultValue(aukeratutakoIzena);
		if(AUKERATUTAKO_HERRIA_ID == EDOZEIN_HERRIA_ID)
			_herriakList.setSummary(R.string.beste_herriak);
		else
			_herriakList.setSummary(aukeratutakoIzena);

		//FontHelper.applyFont(getApplicationContext(), findViewById(_hizkuntzaList.getLayoutResource()), FONT_URL);
		dbc.open();
		ArrayList<DataBaseObject> hizkuntzak = dbc.getObjektuak("hizkuntzak", "ORDER BY id ASC");
		dbc.close();
		
		count = hizkuntzak.size();
		entries = new CharSequence[count];
		entryValues = new CharSequence[count];
		DataBaseObject hizkuntza;
		aukeratutakoIzena = "";
		
		for(int i = 0; i < count; i++)
		{
			hizkuntza = hizkuntzak.get(i);
			
			if(hizkuntza.getID() == AUKERATUTAKO_HIZKUNTZA_ID)
				aukeratutakoIzena = hizkuntza.getIzena();
			
			entries[i] = hizkuntza.getIzena();
			entryValues[i] = String.valueOf(hizkuntza.getID());
		}

		_hizkuntzaList = (ListPreference)getPreferenceManager().findPreference("hizkuntzaPref");
		//FontHelper.applyFont(getApplicationContext(), findViewById(_herriakList.getLayoutResource()), FONT_URL);
		_hizkuntzaList.setOnPreferenceChangeListener(this);
		_hizkuntzaList.setEntries(entries);
		_hizkuntzaList.setEntryValues(entryValues);

		_hizkuntzaList.setDefaultValue(aukeratutakoIzena);
		_hizkuntzaList.setSummary(aukeratutakoIzena);
	}

	public boolean onPreferenceChange(Preference preference, Object newValue)
	{
		String key = preference.getKey();
		boolean itzuli = false;
		if (key.equals(_herriakList.getKey()))
		{
			IS_AUKERAKETA_ALDATUA = true;
			itzuli = true;
			AUKERATUTAKO_HERRIA_ID = Integer.parseInt(newValue.toString());
	        dbc.open();
	        if(AUKERATUTAKO_HERRIA_ID > EDOZEIN_HERRIA_ID)
	        	_herriakList.setSummary(dbc.getHerriIzenaByID(AUKERATUTAKO_HERRIA_ID));
	        else
	        	_herriakList.setSummary(R.string.beste_herriak);
			dbc.setAukeratutakoHerria(AUKERATUTAKO_HERRIA_ID);
	        dbc.close(); 
		}
		else if (key.equals(_hizkuntzaList.getKey()))
		{
			IS_AUKERAKETA_ALDATUA = true;
			itzuli = true;
			AUKERATUTAKO_HIZKUNTZA_ID = Integer.parseInt(newValue.toString());
	        dbc.open();
	        Hizkuntza hizkuntza = dbc.getHizkuntzaByID(AUKERATUTAKO_HIZKUNTZA_ID);
			_hizkuntzaList.setSummary(hizkuntza.getIzena());
			dbc.setAukeratutakoHizkuntza(AUKERATUTAKO_HIZKUNTZA_ID);
	        dbc.close();
	        
	        setToSelectedLocale(getBaseContext());
		}
		
		return itzuli;
	}
	
	static public void setToSelectedLocale(Context context)
	{
	    DataBaseController dbc = new DataBaseController(context);
	    dbc.open();
	    //Settings.AUKERATUTAKO_HIZKUNTZA_ID = dbc.getAukeratutakoHizkuntza();
	    if(Settings.AUKERATUTAKO_HIZKUNTZA_ID != -1)
	    {
			Locale locale = new Locale(dbc.getHizkuntzaByID(Settings.AUKERATUTAKO_HIZKUNTZA_ID).getLaburdura());
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	    }
	    dbc.close();
	}

	public void onBackPressed()
	{
		if(AUKERATUTAKO_HERRIA_ID == -1 || AUKERATUTAKO_HIZKUNTZA_ID == -1)
		{
			Toast.makeText(this, R.string.aukeratu_herria_hizkuntza, Toast.LENGTH_SHORT).show();
		}
		else
			finish();
	}
}
